#!/bin/bash

cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat product/app/webview/webview.apk.* 2>/dev/null >> product/app/webview/webview.apk
rm -f product/app/webview/webview.apk.* 2>/dev/null
cat vendor_bootimg/01_dtbdump_Qualcomm_Technologies,_Inc._SM6150_SoC.dtb.* 2>/dev/null >> vendor_bootimg/01_dtbdump_Qualcomm_Technologies,_Inc._SM6150_SoC.dtb
rm -f vendor_bootimg/01_dtbdump_Qualcomm_Technologies,_Inc._SM6150_SoC.dtb.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
