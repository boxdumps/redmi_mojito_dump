## ancient_mojito-userdebug 12 SQ1D.220205.004 32 release-keys
- Manufacturer: xiaomi
- Platform: sm6150
- Codename: mojito
- Brand: Redmi
- Flavor: ancient_mojito-userdebug
- Release Version: 12
- Id: SQ1D.220205.004
- Incremental: 32
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/ancient_mojito/mojito:12/SQ1D.220205.004/32:userdebug/release-keys
- OTA version: 
- Branch: ancient_mojito-userdebug-12-SQ1D.220205.004-32-release-keys-random-text-323511285721676
- Repo: redmi_mojito_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
